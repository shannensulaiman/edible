import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Maps from "./components/Maps.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Preferences from "./views/Preferences.vue";
import PasswordReset from "./views/PasswordReset.vue";
Vue.use(Router);

const routes = [
  { path: "/location", name: "Maps", component: Maps },
  { path: "/", name: "Home", component: Home },
  { path: "/login", name: "Login", component: Login },
  { path: "/register", name: "Register", component: Register },
  { path: "/preferences", name: "Preferences", component: Preferences },
  { path: "/password-reset", name: "PasswordReset", component: PasswordReset },
];

export default new Router({
  routes,
  mode: "history"
});
